package com.org.dao;


import com.org.model.Country;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountryDao extends BaseRepository<Country, Long>{

    Optional<Country> findByCountryName(String name);

    @Override
    <S extends Country> List<S> save(Iterable<S> entities);
}
