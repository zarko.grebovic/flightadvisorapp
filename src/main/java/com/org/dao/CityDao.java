package com.org.dao;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import com.org.model.City;
import com.org.model.Country;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface CityDao extends BaseRepository<City, Long> {

    Optional<City> findByName(String name);

    @Query(value = "SELECT c.NAME FROM CITY c", nativeQuery = true)
    List<String> findByCityNamesFromDB();

    City findByAirports_Id(Long id);

    City findByAirports_Icao(String icao);

    City findByAirports_Iata(String iata);
}
