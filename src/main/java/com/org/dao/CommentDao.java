package com.org.dao;

import java.math.BigInteger;

import com.org.model.Comment;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentDao extends BaseRepository<Comment, Long> {

}
