package com.org.dao;

import com.org.model.Airport;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AirportDao extends BaseRepository<Airport, Long> {

    List<Airport> findByCityName(String cityName);

    Airport findByIcaoOrIata(String icao, String iata);
}
