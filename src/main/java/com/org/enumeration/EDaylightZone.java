package com.org.enumeration;

public enum EDaylightZone {

    E("Europe"),
    A("US/Canada"),
    S("South America"),
    O("Australia"),
    Z("New Zealand"),
    N("None"),
    U("Unknown");

    private String value;

    EDaylightZone(String zone) {
        this.value = zone;
    }

    public String getZone() {
        return value;
    }

    public static EDaylightZone getDST(String value){
        if(value != null){
        if(value.equals("Europe")){
            return EDaylightZone.E;
        }else if(value.equals("US/Canada")){
            return EDaylightZone.A;
        }else if(value.equals("South America")){
            return EDaylightZone.S;
        }else if(value.equals("Australia")){
            return EDaylightZone.O;
        }else if(value.equals("New Zealand")){
            return EDaylightZone.Z;
        }else if(value.equals("")){
            return EDaylightZone.N;
        }else {
            return EDaylightZone.U;}
        }else{
            return EDaylightZone.U;
        }
    }
}
