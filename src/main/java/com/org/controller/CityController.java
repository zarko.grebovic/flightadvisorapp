package com.org.controller;

import java.math.BigInteger;

import com.org.controller.request.CreateCityReq;
import com.org.model.City;
import com.org.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.exceptions.RecordAlreadyPresentException;
import com.org.exceptions.RecordNotFoundException;

@CrossOrigin("http://localhost:4200")
@ComponentScan(basePackages = "com")
@RestController
@RequestMapping("/city")
public class CityController {

	@Autowired(required= true)
    CityService cityService;

	@PostMapping("/createCity")
	@ExceptionHandler(RecordAlreadyPresentException.class)
	public void addCity(@RequestBody City newCity) {

		cityService.createCity(newCity);
	}

	@GetMapping("/readAllCity")
	public Iterable<City> readAllCitys() {

		return cityService.displayAllCities();
	}

	@PutMapping("/updateCity")
	@ExceptionHandler(RecordNotFoundException.class)
	public void modifyCity(@RequestBody City updateCity) {

		cityService.updateCity(updateCity);
	}

	@GetMapping("/searchCity/{id}")
	@ExceptionHandler(RecordNotFoundException.class)
	public ResponseEntity<?> searchCityByID(@PathVariable("id") Long cityId) {
		return cityService.findCityById(cityId);
	}

	@DeleteMapping("/deleteCity/{id}")
	@ExceptionHandler(RecordNotFoundException.class)
	public void deleteCityByID(@PathVariable("id") Long cityId) {

		cityService.deleteCity(cityId);
	}


}
