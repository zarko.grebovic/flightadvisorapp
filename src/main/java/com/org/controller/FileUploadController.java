package com.org.controller;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.org.helper.AirportImportHelper;
import com.org.model.City;
import com.org.model.Country;
import com.org.service.UploadFileService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileUploadController {

    //String UPLOAD_DIR = "D://upload//";
    @Autowired
    UploadFileService uploadFileService;

    @RequestMapping(value = "/upload-airport", method = RequestMethod.POST)
    public ResponseEntity<Boolean> handleFileUploadAirplane(@RequestParam(value = "file") MultipartFile file) throws IOException {

        if (!file.isEmpty()) {
           Boolean result = uploadFileService.parseFileAirport(file);
           return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }

        return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.OK);
    }

    @RequestMapping(value = "/upload-route", method = RequestMethod.POST)
    public ResponseEntity<Boolean> handleFileUploadRoute(@RequestParam(value = "file") MultipartFile file) throws IOException {

        if (!file.isEmpty()) {
            Boolean result = uploadFileService.parseFileRoutes(file);
            return new ResponseEntity<Boolean>(result, HttpStatus.OK);
        }

        return new ResponseEntity<Boolean>(Boolean.FALSE, HttpStatus.OK);
    }
//    @RequestMapping(value = "/upload/{galleryId}", method = RequestMethod.GET)
//    public ResponseEntity<byte[]> getFile(@PathVariable("galleryId")String galleryId) throws IOException {
//
//    }


    private String getFileExtension(MultipartFile inFile) {
        String fileExtention = inFile.getOriginalFilename().substring(inFile.getOriginalFilename().lastIndexOf('.'));
        return fileExtention;
    }



}