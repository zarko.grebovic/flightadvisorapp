package com.org.controller;

import java.math.BigInteger;

import com.org.model.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.org.exceptions.RecordNotFoundException;
import com.org.service.AirportService;
import com.org.service.CommentService;
import com.org.service.RouteService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/route")
public class RouteController {
	/*
	 * Creating Service object
	 */
	@Autowired
	RouteService routeService;

	@Autowired
	AirportService airportService;

	@Autowired
    CommentService commentService;

	/*
	 * Controller for adding Scheduled Routes
	 */
	@PostMapping("/add")
	public ResponseEntity<Route> addRoute(@ModelAttribute Route scheduledRoute,
									   @RequestParam(name = "srcAirport") String source, @RequestParam(name = "dstnAirport") String destination,
									   @RequestParam(name = "deptDateTime") String departureTime, @RequestParam(name = "arrDateTime") String arrivalTime) {
//		Schedule schedule = new Schedule();
//		schedule.setId(scheduledRoute.getId());
//		try {
//			schedule.setSrcAirport(airportService.viewAirport(source));
//		} catch (RecordNotFoundException e) {
//			return new ResponseEntity("Airport Not Found", HttpStatus.BAD_REQUEST);
//		}
//		try {
//			schedule.setDstnAirport(airportService.viewAirport(destination));
//		} catch (RecordNotFoundException e) {
//			return new ResponseEntity("Airport Not Found", HttpStatus.BAD_REQUEST);
//		}
//		schedule.setDeptDateTime(departureTime);
//		schedule.setArrDateTime(arrivalTime);
//		try {
//			scheduledRoute.setRoute(flightService.viewRoute(scheduledRoute.getId()));
//		} catch (RecordNotFoundException e1) {
//			return new ResponseEntity("Route Not Found", HttpStatus.BAD_REQUEST);
//		}
//		scheduledRoute.setSchedule(schedule);
//		scheduledRoute.setAvailableSeats(scheduledRoute.getRoute().getSeatCapacity());
//		try {
//			return new ResponseEntity<Route>(scheduleRouteService.addRoute(scheduledRoute),
//					HttpStatus.OK);
//		} catch (Exception e) {
//			return new ResponseEntity("Error adding Route." + e, HttpStatus.BAD_REQUEST);
//		}
		return null;
	}

	/*
	 * Controller for modifying existing Scheduled Routes
	 */
	@PutMapping("/modify")
	public ResponseEntity<Route> modifyRoute(@ModelAttribute Route scheduleRoute) {
		Route modifyRoute = routeService.modifyRoute(scheduleRoute);
		if (modifyRoute == null) {
			return new ResponseEntity("Route not modified", HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			return new ResponseEntity<Route>(modifyRoute, HttpStatus.OK);
		}

	}

	/*
	 * Controller for deleting existing Routes
	 */
	@DeleteMapping("/delete")
	public String deleteSF(@RequestParam Long flightId) throws RecordNotFoundException {
		return routeService.removeRoute(flightId);
	}

	/*
	 * Controller for viewing a Scheduled Route by ID
	 */
	@GetMapping("/search")
	@ExceptionHandler(RecordNotFoundException.class)
	public ResponseEntity<Route> viewSF(@RequestParam Long flightId) throws RecordNotFoundException {
		Route route = routeService.viewRoute(flightId);
		if (route == null) {
			return new ResponseEntity("Route not present", HttpStatus.BAD_REQUEST);
		} else {
			return new ResponseEntity<Route>(route, HttpStatus.OK);
		}
	}

	/*
	 * Controller for viewing all Scheduled Routes
	 */
	@GetMapping("/viewAll")
	public Iterable<Route> viewAllRoute() {
		return routeService.viewAllRoutes();
	}
	

}
