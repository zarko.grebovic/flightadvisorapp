package com.org.controller;

import com.org.controller.response.CalculationResponse;
import com.org.helper.FindRoutes;
import com.org.helper.RouteAllItems;
import com.org.model.Airport;
import com.org.model.Route;
import com.org.service.AirportService;
import com.org.service.CalculationService;
import com.org.service.CityService;
import com.org.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.*;



@RestController
@RequestMapping("/flight-calculation")
public class FlightCalculationController {

    @Autowired
    CalculationService calculationService;

    @PostMapping("/calculateRoutesByCities")
    public ResponseEntity<CalculationResponse> calculateRoutesByCities(@RequestBody CalculationByCitiesReqTO request) {

        Map<String, BigDecimal> res = calculationService.createGraph(request.getSourceCityName(), request.getDestinationCityName());

        CalculationResponse response = new CalculationResponse();
        response.getAllCities().add(request.getSourceCityName());
        BigDecimal sum = BigDecimal.ZERO;
        for (Map.Entry<String, BigDecimal> t : res.entrySet()){
            response.getAllCities().add(t.getKey());
            sum = sum.add(t.getValue());
            response.setTotalCost(sum);
        }

        return new ResponseEntity<CalculationResponse>(response, HttpStatus.OK);
    }




}



