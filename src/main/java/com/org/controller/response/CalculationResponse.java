package com.org.controller.response;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CalculationResponse {

    private List<String> allCities = new ArrayList<>();

    private BigDecimal totalCost;

    public List<String> getAllCities() {
        return allCities;
    }

    public void setAllCities(List<String> allCities) {
        this.allCities = allCities;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }
}
