package com.org.controller;

import java.math.BigInteger;

import com.org.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.exceptions.RecordAlreadyPresentException;
import com.org.exceptions.RecordNotFoundException;
import com.org.service.CommentService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/comment")
public class CommentController {
	@Autowired(required = true)
    CommentService commentService;

	@PostMapping("/addComment")
	@ExceptionHandler(RecordAlreadyPresentException.class)
	public void addComment(@RequestBody Comment comment) {
		commentService.addComment(comment);
	}

	@GetMapping("/allComment")
	public Iterable<Comment> viewAllComment() {
		return commentService.viewAllComment();
	}

	@GetMapping("/viewComment/{id}")
	@ExceptionHandler(RecordNotFoundException.class)
	public Comment viewComment(@PathVariable("id") Long commentNo) {
		return commentService.viewComment(commentNo);
	}

	@PutMapping("/updateComment")
	@ExceptionHandler(RecordNotFoundException.class)
	public void modifyComment(@RequestBody Comment comment) {
		commentService.modifyComment(comment);
	}

	@DeleteMapping("/deleteComment/{id}")
	@ExceptionHandler(RecordNotFoundException.class)
	public void removeComment(@PathVariable("id") Long commentNo) {
		commentService.removeComment(commentNo);
	}
}
