package com.org.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A Route.
 */
@Entity
@Table(name = "route")
//@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Route implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "airline")
    private String airline;

    @Column(name = "airline_id")
    private Long airlineID;

    @Column(name = "source_airport")
    private String sourceAirport;

    @Column(name = "source_airport_id")
    private Long sourceAirportID;

    @Column(name = "destination_airport")
    private String destinationAirport;

    @Column(name = "destination_airport_id")
    private Long destinationAirportID;

    @Column(name = "codeshare")
    private String codeshare;

    @Column(name = "stops")
    private Integer stops;

    @Column(name = "equipment")
    private String equipment;

    @Column(name = "price", precision = 21, scale = 2)
    private BigDecimal price;

    @Column(name = "sourceName")
    private String sourceName;

    @Column(name = "destName")
    private String destName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAirline() {
        return airline;
    }

    public Route() {
    }

    public Route airline(String airline) {
        this.airline = airline;
        return this;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public Long getAirlineID() {
        return airlineID;
    }

    public Route airlineID(Long airlineID) {
        this.airlineID = airlineID;
        return this;
    }

    public void setAirlineID(Long airlineID) {
        this.airlineID = airlineID;
    }

    public String getSourceAirport() {
        return sourceAirport;
    }

    public Route sourceAirport(String sourceAirport) {
        this.sourceAirport = sourceAirport;
        return this;
    }

    public void setSourceAirport(String sourceAirport) {
        this.sourceAirport = sourceAirport;
    }

    public Long getSourceAirportID() {
        return sourceAirportID;
    }

    public Route sourceAirportID(Long sourceAirportID) {
        this.sourceAirportID = sourceAirportID;
        return this;
    }

    public void setSourceAirportID(Long sourceAirportID) {
        this.sourceAirportID = sourceAirportID;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public Route destinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
        return this;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public Long getDestinationAirportID() {
        return destinationAirportID;
    }

    public Route destinationAirportID(Long destinationAirportID) {
        this.destinationAirportID = destinationAirportID;
        return this;
    }

    public void setDestinationAirportID(Long destinationAirportID) {
        this.destinationAirportID = destinationAirportID;
    }

    public String getCodeshare() {
        return codeshare;
    }

    public Route codeshare(String codeshare) {
        this.codeshare = codeshare;
        return this;
    }

    public void setCodeshare(String codeshare) {
        this.codeshare = codeshare;
    }

    public Integer getStops() {
        return stops;
    }

    public Route stops(Integer stops) {
        this.stops = stops;
        return this;
    }

    public void setStops(Integer stops) {
        this.stops = stops;
    }

    public String getEquipment() {
        return equipment;
    }

    public Route equipment(String equipment) {
        this.equipment = equipment;
        return this;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Route price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getDestName() {
        return destName;
    }

    public void setDestName(String destName) {
        this.destName = destName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Route)) {
            return false;
        }
        return id != null && id.equals(((Route) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Route{" +
            "id=" + getId() +
            ", airline='" + getAirline() + "'" +
            ", airlineID=" + getAirlineID() +
            ", sourceAirport='" + getSourceAirport() + "'" +
            ", sourceAirportID='" + getSourceAirportID() + "'" +
            ", destinationAirport='" + getDestinationAirport() + "'" +
            ", destinationAirportID='" + getDestinationAirportID() + "'" +
            ", codeshare='" + getCodeshare() + "'" +
            ", stops=" + getStops() +
            ", equipment='" + getEquipment() + "'" +
            ", price=" + getPrice() +
            "}";
    }
}
