package com.org.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.org.enumeration.EDaylightZone;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZoneId;

/**
 * A Airport.
 */
@Entity
@Table(name = "airport")
public class Airport implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "iata")
    private String iata;

    @Column(name = "icao")
    private String icao;

    @Column(name = "latitude")
    private Float latitude;

    @Column(name = "longitude")
    private Float longitude;

    @Column(name = "altitude")
    private Float altitude;

    @Column(name = "timezone")
    private String timezone;

    @Enumerated(EnumType.STRING)
    @Column(name = "dst")
    private EDaylightZone dst;

    @Column(name = "tz_db")
    private ZoneId tz;

    @Column(name = "type")
    private String type;

    @Column(name = "source")
    private String source;

    @ManyToOne
    @JsonIgnoreProperties(value = "airports", allowSetters = true)
    private City city;

    public Airport() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Airport name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIata() {
        return iata;
    }

    public Airport iata(String iata) {
        this.iata = iata;
        return this;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public Airport icao(String icao) {
        this.icao = icao;
        return this;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public Float getLatitude() {
        return latitude;
    }

    public Airport latitude(Float latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public Airport longitude(Float longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getAltitude() {
        return altitude;
    }

    public Airport altitude(Float altitude) {
        this.altitude = altitude;
        return this;
    }

    public void setAltitude(Float altitude) {
        this.altitude = altitude;
    }

    public String getTimezone() {
        return timezone;
    }

    public Airport timezone(String timezone) {
        this.timezone = timezone;
        return this;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public EDaylightZone getDst() {
        return dst;
    }

    public void setDst(EDaylightZone dst) {
        this.dst = dst;
    }

    public ZoneId getTz() {
        return tz;
    }

    public void setTz(ZoneId tz) {
        this.tz = tz;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public City getCity() {
        return city;
    }

    public Airport city(City city) {
        this.city = city;
        return this;
    }

    public void setCity(City city) {
        this.city = city;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Airport)) {
            return false;
        }
        return name != null && name.equals(((Airport) o).name);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Airport{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", iata='" + getIata() + "'" +
            ", icao='" + getIcao() + "'" +
            ", latitude=" + getLatitude() +
            ", longitude=" + getLongitude() +
            ", altitude=" + getAltitude() +
            ", timezone='" + getTimezone() + "'" +
            "}";
    }
}
