package com.org.model;


import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A City.
 */
@Entity
@Table(name = "city")
public class City implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "number_of_comments")
    private Integer numberOfComments;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Country country;

    @OneToMany(mappedBy = "city")
    private Set<Comment> comments = new HashSet<>();

    @OneToMany(mappedBy = "city")
//    @NotFound(action = NotFoundAction.IGNORE)
    private Set<Airport> airports = new HashSet<>();

    public City(String name, Country country, String description) {
        this.name = name;
        this.country = country;
        this.description = description;
    }

    public City() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public City name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public City description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getNumberOfComments() {
        return numberOfComments;
    }

    public City numberOfComments(Integer numberOfComments) {
        this.numberOfComments = numberOfComments;
        return this;
    }

    public void setNumberOfComments(Integer numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public Country getCountry() {
        return country;
    }

    public City country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public City comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public City addComment(Comment comment) {
        this.comments.add(comment);
        comment.setCity(this);
        return this;
    }

    public City removeComment(Comment comment) {
        this.comments.remove(comment);
        comment.setCity(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Airport> getAirports() {
        return airports;
    }

    public City airports(Set<Airport> airports) {
        this.airports = airports;
        return this;
    }

    public City addAirport(Airport airport) {
        this.airports.add(airport);
        airport.setCity(this);
        return this;
    }

    public City removeAirport(Airport airport) {
        this.airports.remove(airport);
        airport.setCity(null);
        return this;
    }

    public void setAirports(Set<Airport> airports) {
        this.airports = airports;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof City)) {
            return false;
        }
        return name != null && name.equals(((City) o).name);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "City{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", numberOfComments=" + getNumberOfComments() +
            "}";
    }
}
