package com.org.service;

import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {

    Boolean parseFileAirport(MultipartFile file);

    Boolean parseFileRoutes(MultipartFile file);
}
