package com.org.service.impl;

import com.org.dao.CountryDao;
import com.org.exceptions.RecordAlreadyPresentException;
import com.org.exceptions.RecordNotFoundException;
import com.org.model.Country;
import com.org.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class CountryServiceImpl implements CountryService {

    /*
     * Creating DAO object
     */
    @Autowired
    CountryDao countryDao;

    /*
     * making new Country
     */
    @Override
    public ResponseEntity<Country> createCountry(Country newCountry) {

        Optional<Country> findCountryById = countryDao.findById(newCountry.getId());
        try {
            if (!findCountryById.isPresent()) {
                countryDao.save(newCountry);
                return new ResponseEntity<Country>(newCountry, HttpStatus.OK);
            } else
                throw new RecordAlreadyPresentException(
                        "Country with Country Id: " + newCountry.getId() + " already exists!!");
        } catch (RecordAlreadyPresentException e) {

            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    /*
     * update country made
     */
    @Override
    public Country updateCountry(Country changedCountry) {
        Optional<Country> findCountryById = countryDao.findById(changedCountry.getId());
        if (findCountryById.isPresent()) {
            countryDao.save(changedCountry);
        } else
            throw new RecordNotFoundException(
                    "Country with Country Id: " + changedCountry.getId() + " not exists!!");
        return changedCountry;
    }

    /*
     * deleteing the country
     */

    @Override
    public String deleteCountry(Long countryId) {

        Optional<Country> findCountryById = countryDao.findById(countryId);
        if (findCountryById.isPresent()) {
            countryDao.deleteById(countryId);
            return "Country Deleted!!";
        } else
            throw new RecordNotFoundException("Country not found for the entered CountryID");
    }

    /*
     * (non-Javadoc)
     *
     * @see com.org.service.CountryService#displayAllCountry() show all country
     */
    @Override
    public Iterable<Country> displayAllCities() {

        return countryDao.findAll();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.org.service.CountryService#findCountryById(java.math.Long)
     * find country by ID
     */
    @Override
    public ResponseEntity<?> findCountryById(Long countryId) {
        Optional<Country> findById = countryDao.findById(countryId);
        try {
            if (findById.isPresent()) {
                Country findCountry = findById.get();
                return new ResponseEntity<Country>(findCountry, HttpStatus.OK);
            } else
                throw new RecordNotFoundException("No record found with ID " + countryId);
        } catch (RecordNotFoundException e) {
            return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public List<Country> saveCoutnries(Set countries){
        return countryDao.save(countries);
     }

     @Override
     public Country save(Country country){
        return countryDao.save(country);
     }

     @Override
     public Country findByName(String countryName) throws RecordNotFoundException{
        Optional<Country> retVal = countryDao.findByCountryName(countryName);
        if(retVal.isPresent()){
            return retVal.get();
        }else{
            throw new RecordNotFoundException("No record for countryName");
        }
     }
}
