package com.org.service.impl;

import com.org.dao.CommentDao;
import com.org.exceptions.*;
import java.util.Optional;

import com.org.model.Comment;
import com.org.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.org.exceptions.RecordAlreadyPresentException;

@Service
public class CommentServiceImpl implements CommentService {
	@Autowired
	CommentDao commentDao;

	/*
	 * add a comment
	 */
	@Override
	public ResponseEntity<Comment> addComment(Comment comment) {
		Optional<Comment> findById = commentDao.findById(comment.getId());
		try {
		if (!findById.isPresent()) {
			commentDao.save(comment);
			return new ResponseEntity<Comment>(comment,HttpStatus.OK);
		} else
			throw new RecordAlreadyPresentException("Comment with number: " + comment.getId() + " already present");
	}
		catch(RecordAlreadyPresentException e)
		{
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	/*
	 * view all comments
	 */
	@Override
	public Iterable<Comment> viewAllComment() {
		return commentDao.findAll();
	}

	/*
	 * search a comment
	 */
	@Override
	public Comment viewComment(Long commentNumber) {
		Optional<Comment> findById = commentDao.findById(commentNumber);
		if (findById.isPresent()) {
			return findById.get();
		}
		else
			throw new RecordNotFoundException("Comment with number: " + commentNumber + " not exists");
	    }
		/*catch(RecordNotFoundException e)
		{
			return new ResponseEntity(e.getMessage(),HttpStatus.NOT_FOUND);
		}*/

	/*
	 * modify a comment
	 */
	@Override
	public Comment modifyComment(Comment comment) {
		Optional<Comment> findById = commentDao.findById(comment.getId());
		if (findById.isPresent()) {
			commentDao.save(comment);
		} else
			throw new RecordNotFoundException("Comment with number: " + comment.getId() + " not exists");
		return comment;
	}

	/*
	 * remove a comment
	 */
	public String removeComment(Long commentNumber) {
		Optional<Comment> findById = commentDao.findById(commentNumber);
		if (findById.isPresent()) {
			commentDao.deleteById(commentNumber);
			return "Comment removed!!";
		} else
			throw new RecordNotFoundException("Comment with number: " + commentNumber + " not exists");

	}
}
