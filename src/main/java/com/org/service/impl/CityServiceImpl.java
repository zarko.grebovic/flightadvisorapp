package com.org.service.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import com.org.controller.request.CreateCityReq;
import com.org.dao.CityDao;
import com.org.model.City;
import com.org.model.Country;
import com.org.service.CityService;
import com.org.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.org.dao.CommentDao;
import com.org.exceptions.RecordAlreadyPresentException;
import com.org.exceptions.RecordNotFoundException;

@Service
public class CityServiceImpl implements CityService {

	/*
	 * Creating DAO object
	 */
	@Autowired
	CityDao cityDao;

	@Autowired
	CountryService countryService;
	/*
	 * making new City
	 */
	@Override
	public ResponseEntity<City> createCity(City newCity) {

		Optional<City> findCityById = cityDao.findById(newCity.getId());
		try {
			if (!findCityById.isPresent()) {
				cityDao.save(newCity);
				return new ResponseEntity<City>(newCity, HttpStatus.OK);
			} else
				throw new RecordAlreadyPresentException(
						"City with City Id: " + newCity.getId() + " already exists!!");
		} catch (RecordAlreadyPresentException e) {

			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	/*
	 * update city made
	 */
	@Override
	public City updateCity(City changedCity) {
		Optional<City> findCityById = cityDao.findById(changedCity.getId());
		if (findCityById.isPresent()) {
			cityDao.save(changedCity);
		} else
			throw new RecordNotFoundException(
					"City with City Id: " + changedCity.getId() + " not exists!!");
		return changedCity;
	}

	/*
	 * deleteing the city
	 */

	@Override
	public String deleteCity(Long cityId) {

		Optional<City> findCityById = cityDao.findById(cityId);
		if (findCityById.isPresent()) {
			cityDao.deleteById(cityId);
			return "City Deleted!!";
		} else
			throw new RecordNotFoundException("City not found for the entered CityID");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.CityService#displayAllCity() show all city
	 */
	@Override
	public Iterable<City> displayAllCities() {

		return cityDao.findAll();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.org.service.CityService#findCityById(java.math.Long)
	 * find city by ID
	 */
	@Override
	public ResponseEntity<?> findCityById(Long cityId) {
		Optional<City> findById = cityDao.findById(cityId);
		try {
			if (findById.isPresent()) {
				City findCity = findById.get();
				return new ResponseEntity<City>(findCity, HttpStatus.OK);
			} else
				throw new RecordNotFoundException("No record found with ID " + cityId);
		} catch (RecordNotFoundException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public City save(City city){
		return cityDao.save(city);
	}

	@Override
	public City findByName(String cityName) throws RecordNotFoundException{
		Optional<City> retVal = cityDao.findByName(cityName);
		if(retVal.isPresent()){
			return retVal.get();
		}else{
			throw new RecordNotFoundException("No record for cityName");
		}
	}

	@Override
	public List<String> findByCityNamesFromDB(){
		return cityDao.findByCityNamesFromDB();
	}

	@Override
	public List<City> saveCities(List<City> cities){
		return cityDao.save(cities);
	}

	@Override
	public City findByAirportId(Long airportId){
		return cityDao.findByAirports_Id(airportId);
	}

	@Override
	public City findByAirportIcao(String icao){
		return cityDao.findByAirports_Icao(icao);
	}

	@Override
	public City findByAirportIata(String iata){
		return cityDao.findByAirports_Iata(iata);
	}



}
