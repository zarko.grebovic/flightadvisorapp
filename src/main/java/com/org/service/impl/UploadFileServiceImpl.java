package com.org.service.impl;

import com.org.enumeration.EDaylightZone;
import com.org.helper.AirportImportHelper;
import com.org.helper.RouteImportHelper;
import com.org.model.Airport;
import com.org.model.City;
import com.org.model.Country;
import com.org.model.Route;
import com.org.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.ZoneId;
import java.util.*;

@Service
public class UploadFileServiceImpl implements UploadFileService {

    private static String DELIMITER_1 = ",";
    private static String DELIMITER_2 = "\\n";
    private static String DELIMITER_3 = "\\r\\n";
    private static String DELIMITER_4 = "\"";
    private static String DELIMITER_5 = "\\N";

    private static String UTC = "UTC";

    @Autowired
    CityService cityService;

    @Autowired
    CountryService countryService;

    @Autowired
    AirportService airportService;

    @Autowired
    RouteService routeService;

    private static HashMap<Long, String> allCityAirportIds = new HashMap<Long, String>();
    private static HashMap<String, String> allCityAirportIcao = new HashMap<String, String>();
    private static HashMap<String, String> allCityAirportIata = new HashMap<String, String>();

   public Boolean parseFileAirport(MultipartFile file) {

       List<String> allCitiesFromDB = cityService.findByCityNamesFromDB();
       List<Airport> airportsForDB = new LinkedList<Airport>();
       Set<Country> allCountriesFromFile = new HashSet<Country>();
       Set<City> allCitiesFromFile = new HashSet<City>();
       List<City> listForSave = new ArrayList<City>();
       Scanner sc;
       try {
           sc = new Scanner(file.getInputStream());
       } catch (IOException e) {
           return false;
       }
       sc.useDelimiter(DELIMITER_2);
       int i = 0;
       while (sc.hasNext()) {
           String row = sc.next();
           String[] airports = row.split(DELIMITER_1);
           //Save all countries
           if(!StringUtils.isEmpty(airports[3]) && !StringUtils.isEmpty(airports[2])) {

               /////////////////////////
               int countOfCoutries = allCountriesFromFile.size();
               Country cnt = new Country(validateField(airports[3]));
               allCountriesFromFile.add(cnt);
               if (countOfCoutries == allCountriesFromFile.size()) {
                   for (Iterator<Country> it = allCountriesFromFile.iterator(); it.hasNext(); ) {
                       Country c = it.next();
                       if (c.equals(new Country(validateField(airports[3])))) ;
                       cnt = c;
                   }
               }

               int countOfCities = allCountriesFromFile.size();
               City city = new City(validateField(airports[2]), cnt, "Generated from file");
               allCitiesFromFile.add(city);
               if (countOfCities == allCitiesFromFile.size()) {
                   for (Iterator<City> it = allCitiesFromFile.iterator(); it.hasNext(); ) {
                       City c = it.next();
                       if (c.equals(city));
                       city = c;
                   }
               }else{
                     allCityAirportIds.put(Long.parseLong(airports[0]), city.getName());
                     allCityAirportIata.put(validateField(airports[4]), city.getName());
                     allCityAirportIata.put(validateField(airports[5]), city.getName());
               }
               AirportImportHelper aih = new AirportImportHelper(
                       Long.parseLong(airports[0]),
                       validateField(airports[1]),
                       validateField(airports[2]),
                       validateField(airports[3]),
                       validateField(airports[4]),
                       validateField(airports[5]),
                       validateNumberField(airports[6]),
                       validateNumberField(airports[7]),
                       validateNumberField(airports[8]),
                       validateField(airports[9]),
                       validateField(airports[10]),
                       validateTzField(airports[11]),
                       validateField(airports[12]),
                       validateField(airports[13]));

               Airport forSave = mapAirportHandlerToAirport(aih);
               forSave.setCity(city);
               city.getAirports().add(forSave);
               listForSave.add(city);
               airportsForDB.add(forSave);

           }
//       }
   }
       countryService.saveCoutnries(allCountriesFromFile);
       cityService.saveCities(listForSave);
       airportService.saveAirports(airportsForDB);

       return true;
    }

    public Boolean parseFileRoutes(MultipartFile file){
        List<Route> routesForDB = new LinkedList<Route>();
        Scanner sc = null;
        try {
            sc = new Scanner(file.getInputStream());
        } catch (IOException e) {
            return false;
        }
        sc.useDelimiter(DELIMITER_3);
        while(sc.hasNext()) {
            String row = sc.next();

            String[] airports = row.split(DELIMITER_1);

            String sourceName;
            String destName;

            if(validateLongField(airports[3]) != null){
                sourceName = allCityAirportIds.get(validateLongField(airports[3]));
            }else if(validateField(airports[2]) != null && validateField(airports[2]).length() == 3){
                sourceName = allCityAirportIata.get(validateField(airports[2]));
            }else{
                sourceName = allCityAirportIcao.get(validateField(airports[2]));
            }

            if(validateLongField(airports[5]) != null){
                destName = allCityAirportIds.get(validateLongField(airports[5]));
            }else if(validateField(airports[4]) != null && validateField(airports[4]).length() == 3){
                destName = allCityAirportIata.get(validateField(airports[4]));
            }else{
                destName = allCityAirportIcao.get(validateField(airports[4]));
            }

            if(StringUtils.isEmpty(sourceName) || StringUtils.isEmpty(destName)){
                continue;
            }

            RouteImportHelper aih = new RouteImportHelper(
                    validateField(airports[0]),
                    validateLongField(airports[1]),
                    validateField(airports[2]),
                    validateLongField(airports[3]),
                    validateField(airports[4]),
                    validateLongField(airports[5]),
                    validateField(airports[6]),
                    Integer.parseInt(airports[7]),
                    validateField(airports[8]),
                    new BigDecimal(airports[9]));

            Route forSave = mapRouteHandlerToRoute(aih);
            forSave.setSourceName(sourceName);
            forSave.setDestName(destName);

            routesForDB.add(forSave);
        }
        routeService.saveRoutes(routesForDB);


        return true;
    }

    private Airport mapAirportHandlerToAirport(AirportImportHelper helper){
       Airport mapped = new Airport();
       mapped.setId(helper.getId());
       mapped.setAltitude(helper.getAltitude());
       mapped.setIata(helper.getIata());
       mapped.setIcao(helper.getIcao());
       mapped.setLatitude(helper.getLatitude());
       mapped.setLongitude(helper.getLongitude());
       mapped.setName(helper.getName());
       mapped.setSource(helper.getSource());
       mapped.setType(helper.getType());
       mapped.setTimezone(UTC + helper.getTimezone());
       mapped.setTz(ZoneId.of(validateTzField(helper.getTz())));
       mapped.setDst(EDaylightZone.getDST(helper.getDst()));
       return mapped;
    }

    private Route mapRouteHandlerToRoute(RouteImportHelper helper){
        Route mapped = new Route();
        //mapped.setId(helper.getId());
        mapped.setAirline(helper.getAirline());
        mapped.setAirlineID(helper.getAirlineID());
        mapped.setCodeshare(helper.getCodeshare());
        mapped.setDestinationAirport(helper.getDestinationAirport());
        mapped.setDestinationAirportID(helper.getDestinationAirportID());
        mapped.setEquipment(helper.getEquipment());
        mapped.setPrice(helper.getPrice());
        mapped.setSourceAirport(helper.getSourceAirport());
        mapped.setSourceAirportID(helper.getSourceAirportID());
        mapped.setStops(helper.getStops());
        return mapped;
    }

    private String validateField(String input){

        if(input.startsWith(DELIMITER_4) && input.endsWith(DELIMITER_4)){
            String cleanInput = input.substring(1, input.length()-1);
            return cleanInput;
        }else if(input.equals(DELIMITER_5)){
            return null;
        }else{
            return input;
        }

    }

    private boolean isValidCity(String cityName){
        if(StringUtils.isEmpty(cityName)){
            return false;
        }else if(cityName.equals(DELIMITER_5)){
            return false;
        }
        return true;

    }

    private String validateTzField(String input){

        if(!StringUtils.isEmpty(input) && input.startsWith(DELIMITER_4) && input.endsWith(DELIMITER_4)){
            String cleanInput = input.substring(1, input.length()-1);
            return cleanInput;
        }else if(StringUtils.isEmpty(input)){
            return UTC;
        }else if(input.equals(DELIMITER_5)){
            return UTC;
        }else if(input.length() < 3){
            return UTC;
        }else{
            return input;
        }
    }

    private Float validateNumberField(String input){

        if(input.startsWith(DELIMITER_4) && input.endsWith(DELIMITER_4)){
            String cleanInput = input.substring(1, input.length()-1);
            try {
                Float retVal = Float.parseFloat(cleanInput);
                return retVal;
            }catch (NumberFormatException e){
                return null;
            }

    }else{return Float.parseFloat(input);}
}

    private Long validateLongField(String input){

       if(input == null || input.equals(DELIMITER_5)){
           return null;
       }

        if(input.startsWith(DELIMITER_4) && input.endsWith(DELIMITER_4)){
            String cleanInput = input.substring(1, input.length()-1);
            try {
                Long retVal = Long.parseLong(cleanInput);
                return retVal;
            }catch (NumberFormatException e){
                return null;
            }

        }else{
            return Long.parseLong(input);
        }
    }
}
