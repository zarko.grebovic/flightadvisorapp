package com.org.service.impl;

import com.org.helper.FindRoutes;
import com.org.helper.RouteAllItems;
import com.org.model.Airport;
import com.org.model.Route;
import com.org.service.AirportService;
import com.org.service.CalculationService;
import com.org.service.CityService;
import com.org.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

@Service
public class CalculationServiceImpl implements CalculationService {

    @Autowired
    RouteService routeService;

    @Autowired
    AirportService airportService;

    @Autowired
    CityService cityService;

    public RouteAllItems<String> getAllCities(Set<String> cities, Set<String> restrictedCities, RouteAllItems<String> graphFindAllPaths) {
        Set newListCities = new HashSet<String>();
        for (String city : cities) {
            for (Airport a : airportService.findByCityName(city)) {
                //City c = cityService.findByName(city);
                List<Route> list = routeService.findBySourceAirport(a.getIata());
                list.addAll(routeService.findBySourceAirport(a.getIcao()));
                for (Route r : list) {
                    Airport ap = airportService.findByIcaoOrIata(r.getDestinationAirport(), r.getDestinationAirport());
                    String destCityName = null;
                    if (ap != null) {
                        destCityName = ap.getCity().getName();
                    } else {
                        continue;
                    }

                    if (!restrictedCities.contains(destCityName)) {
                        restrictedCities.add(destCityName);
                        graphFindAllPaths.addNode(destCityName);
                        graphFindAllPaths.addEdge(city, destCityName, r.getPrice());
                        newListCities.add(destCityName);
                    }
                }
            }
        }
        if (!cities.isEmpty()) {
            getAllCities(newListCities, restrictedCities, graphFindAllPaths);
        }
        return graphFindAllPaths;

    }

    public Map<String, BigDecimal> createGraph(String source, String destination) {
        RouteAllItems<String> graphFindAllPaths = new RouteAllItems<String>();
        for (Iterator<Airport> it = airportService.viewAllAirport().iterator(); it.hasNext(); ) {
            Airport c = it.next();
            graphFindAllPaths.addNode(c.getCity().getName());

        }
        Iterator<Route> it = routeService.viewAllRoutes().iterator();
        while(it.hasNext()) {
            Route c = it.next();
            graphFindAllPaths.addEdge(c.getSourceName(), c.getDestName(), c.getPrice());
        }
        FindRoutes<String> findAllPaths = new FindRoutes<String>(graphFindAllPaths);
        Map<String, BigDecimal> result =  findAllPaths.getCheapestPath(source, destination);

        return result;

    }
}
