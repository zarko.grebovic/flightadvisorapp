package com.org.service.impl;

import java.util.List;
import java.util.Optional;

import com.org.dao.RouteDao;
import com.org.model.Route;
import com.org.service.CityService;
import com.org.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.org.dao.CityDao;
import com.org.exceptions.RecordNotFoundException;

@Service
public class RouteServiceImpl implements RouteService {

	/*
	 * Creating DAO object
	 */
	@Autowired
	RouteDao dao;

	@Autowired
	CityDao cityDao;

	@Autowired
	CityService cityService;

	/*
	 * Service method to add new Scheduled flight to database
	 */
	@Override
	public Route addRoute(Route route) {
		return dao.save(route);
	}

	/*
	 * Service method to modify existing Scheduled flight in database
	 */
	@Override
	public Route modifyRoute(Route scheduleFlight) {
		Route updateScheduleFlight = dao.findById(scheduleFlight.getId()).get();
//		Schedule updateSchedule = scheduleDao.findById(scheduleFlight.getSchedule().getId()).get();
//		updateScheduleFlight.setAvailableSeats(scheduleFlight.getAvailableSeats());
//		updateSchedule.setSrcAirport(scheduleFlight.getSchedule().getSrcAirport());
//		updateSchedule.setDstnAirport(scheduleFlight.getSchedule().getDstnAirport());
//		updateSchedule.setArrDateTime(scheduleFlight.getSchedule().getArrDateTime());
//		updateSchedule.setDeptDateTime(scheduleFlight.getSchedule().getDeptDateTime());
//		dao.save(updateScheduleFlight);
//		return scheduleFlight;
		return null;
	}

	/*
	 * Service method to remove existing Scheduled flight from database
	 */
	@Override
	public String removeRoute(Long flightId) throws RecordNotFoundException {
//		if (flightId == null)
//			throw new RecordNotFoundException("Enter flight Id");
//		Optional<Route> scheduleFlight = dao.findById(flightId);
//		if (!scheduleFlight.isPresent())
//			throw new RecordNotFoundException("Enter a valid Flight Id");
//		else {
//			dao.deleteById(flightId);
//		}
		return "Scheduled flight with ID " + flightId + " is not found";
	}


	@Override
	public Iterable<Route> viewAllRoutes() {
		return dao.findAll();
	}


	@Override
	public Route viewRoute(Long id) throws RecordNotFoundException {
		if (id == null)
			throw new RecordNotFoundException("Enter flight Id");
		Optional<Route> route = dao.findById(id);
		if (!route.isPresent())
			throw new RecordNotFoundException("Enter a valid Flight Id");
		else
			return route.get();
	}

	@Override
	public List<Route> saveRoutes(List<Route> routes){
		return dao.save(routes);
	}

	@Override
	public List<Route> findBySourceAirport(String sourceAirport){
		return dao.findBySourceAirport(sourceAirport);
	}

}
