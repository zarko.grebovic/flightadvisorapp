package com.org.service;

import java.math.BigInteger;
import java.util.List;


import com.org.exceptions.RecordNotFoundException;
import com.org.model.Route;

public interface RouteService {

	public Route addRoute(Route route);

	public Route modifyRoute(Route route);

	public String removeRoute(Long id) throws RecordNotFoundException;

	public Iterable<Route> viewAllRoutes();

	public Route viewRoute(Long id) throws RecordNotFoundException;

	public List<Route> saveRoutes(List<Route> routes);

	public List<Route> findBySourceAirport(String sourceAirport);

}
