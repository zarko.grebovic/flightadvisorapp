package com.org.service;

import com.org.helper.RouteAllItems;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Set;

public interface CalculationService {

     Map<String, BigDecimal> createGraph(String source, String destination);

     RouteAllItems<String> getAllCities(Set<String> cities, Set<String> restrictedCities, RouteAllItems<String> graphFindAllPaths);
}
