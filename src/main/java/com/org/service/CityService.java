package com.org.service;

import java.math.BigInteger;
import java.util.List;

import com.org.controller.request.CreateCityReq;
import com.org.exceptions.RecordNotFoundException;
import com.org.model.City;
import org.springframework.http.ResponseEntity;

public interface CityService {

	 ResponseEntity<?> createCity(City newCity);

	 City updateCity(City newCity);

	 String deleteCity(Long cityId);

	 Iterable<City> displayAllCities();

	 ResponseEntity<?> findCityById(Long cityId);

	// List<City> saveCities(List cities);

	 City save(City city);

	 City findByName(String cityName) throws RecordNotFoundException;

	 List<String> findByCityNamesFromDB();

	public List<City> saveCities(List<City> cities);

	public City findByAirportId(Long airportId);

	public City findByAirportIcao(String icao);

	public City findByAirportIata(String iata);


}
