package com.org.service;

import com.org.model.Airport;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface AirportService {
	Iterable<Airport> viewAllAirport();

	 Airport viewAirport(Long id);

	 ResponseEntity<?> addAirport(Airport airport);

	 Airport modifyAirport(Airport airport);

	 String removeAirport(Long id);

	 List<Airport> saveAirports(List airports);

	 List<Airport> findByCityName(String cityName);

	Airport findByIcaoOrIata(String icao, String iata);
}
