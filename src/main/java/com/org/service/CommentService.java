package com.org.service;

import java.math.BigInteger;

import com.org.model.Comment;
import org.springframework.http.ResponseEntity;

public interface CommentService {
	public ResponseEntity<?> addComment(Comment comment);

	public Iterable<Comment> viewAllComment();

	public Comment viewComment(Long commentId);

	public Comment modifyComment(Comment comment);

	public String removeComment(Long commentId);

}
