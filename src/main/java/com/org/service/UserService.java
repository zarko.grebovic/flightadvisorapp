package com.org.service;

import java.math.BigInteger;

import com.org.model.User;
import org.springframework.http.ResponseEntity;

public interface UserService {

	public ResponseEntity<?> createUser(User newUser);

	public User updateUser(User newUser);

	public String deleteUser(Long id);

	public Iterable<User> displayAllUser();

	public ResponseEntity<?> findUserById(Long id);
}