package com.org.service;

import com.org.exceptions.RecordNotFoundException;
import com.org.model.Country;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Set;


public interface CountryService {

    ResponseEntity<?> createCountry(Country newCountry);

    Country updateCountry(Country newCountry);

    String deleteCountry(Long id);

    Iterable<Country> displayAllCities();

    ResponseEntity<?> findCountryById(Long id);

    List<Country> saveCoutnries(Set countries);

    Country findByName(String countryName) throws RecordNotFoundException;

    Country save(Country country);
}
