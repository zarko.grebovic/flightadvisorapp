package com.org.helper;

import java.math.BigDecimal;
import java.util.*;

public class FindRoutes<T> {

    private final RouteAllItems<T> graph;

    /**
     * Takes in a graph. This graph should not be changed by the client
     */
    public FindRoutes(RouteAllItems<T> graph) {
        if (graph == null) {
            throw new NullPointerException("The input graph cannot be null.");
        }
        this.graph = graph;
    }

    private void validate (T source, T destination) {

        if (source == null) {
            throw new NullPointerException("The source: " + source + " cannot be  null.");
        }
        if (destination == null) {
            throw new NullPointerException("The destination: " + destination + " cannot be  null.");
        }
        if (source.equals(destination)) {
            throw new IllegalArgumentException("The source and destination: " + source + " cannot be the same.");
        }
    }

    /**
     * Returns the list of paths, where path itself is a list of nodes.
     *
     * @param source            the source node
     * @param destination       the destination node
     * @return                  List of all paths
     */
    public Map<T, BigDecimal> getCheapestPath(T source, T destination) {
        validate(source, destination);

        Map<T, Map<T, BigDecimal>> paths = new HashMap<>();
        Map<T, BigDecimal> pathsPerAllRute = new HashMap<>();
        Map<T, BigDecimal> pathsResult = new HashMap<>();
        List<BigDecimal> minAmount = new ArrayList<BigDecimal>();
        minAmount.add(new BigDecimal(0));
        List<BigDecimal> sumAmount = new ArrayList<BigDecimal>();
        sumAmount.add(new BigDecimal(0));
        int i = 0;
        recursive(source, BigDecimal.ZERO, destination, paths, pathsPerAllRute, new HashMap<T, BigDecimal>(), minAmount, sumAmount, pathsResult);
        return pathsResult;

    }

    // so far this dude ignore's cycles.
    private void recursive (T current,BigDecimal price, T destination,Map<T, Map<T, BigDecimal>> paths, Map<T, BigDecimal> pathsPerAllRute, Map<T, BigDecimal> path, List<BigDecimal> minAmount, List<BigDecimal> sumAmount,Map<T, BigDecimal> pathsResult) {
        path.put(current , price);
        Map currentPath = new HashMap<T, BigDecimal>();
        pathsPerAllRute.put(current, price);

        if (current.equals(destination)) {
            Map pathPrice = new HashMap<T, BigDecimal>();
            pathPrice.put(current, new HashMap(pathsPerAllRute));
            if(!paths.isEmpty()){
                paths.remove(paths.get(0));
            }
            paths.put(current, pathPrice);
            pathsResult.clear();
            Iterator it = pathsPerAllRute.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                pathsResult.put((T)pair.getKey(), (BigDecimal)pair.getValue());
            }
            //pathResult = new HashMap(pathsPerAllRute);
            BigDecimal sumAm = sumAmount.get(0);
            minAmount.remove(minAmount.get(0));
            minAmount.add(sumAm);
            sumAmount.remove(sumAmount.get(0));
            sumAmount.add(new BigDecimal(0));
//            minAmount += sumAmount;
//            sumAmount -= sumAmount;
            pathsPerAllRute.clear();
            path.remove(current);
            return;
        }

        final Set<Map.Entry<T, BigDecimal >> edges  = graph.edgesFrom(current).entrySet();

        for (Map.Entry<T, BigDecimal> t : edges) {
            if (!path.containsKey(t.getKey())) {
                BigDecimal sumAm = sumAmount.get(0).add(t.getValue());
                sumAmount.remove(sumAmount.get(0));
                sumAmount.add(sumAm);
                if(minAmount.get(0).compareTo(BigDecimal.ZERO) != 0 && sumAmount.get(0).compareTo(minAmount.get(0)) == 1 || pathsPerAllRute.size() > 300){
                    continue;
                }else if(pathsPerAllRute.size() > 50){
                    pathsPerAllRute.clear();
                    continue;
                }

                recursive (t.getKey(),t.getValue(), destination, paths, pathsPerAllRute, path, minAmount, sumAmount, pathsResult);
            }
        }

        path.remove(current);
    }


    }

