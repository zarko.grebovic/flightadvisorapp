package com.org.helper;

import java.math.BigDecimal;

public class RouteImportHelper {

    private Long id;

    private String airline;

    private Long airlineID;

    private String sourceAirport;

    private Long sourceAirportID;

    private String destinationAirport;

    private Long destinationAirportID;

    private String codeshare;

    private Integer stops;

    private String equipment;

    private BigDecimal price;

    public RouteImportHelper(String airline, Long airlineID, String sourceAirport, Long sourceAirportID, String destinationAirport, Long destinationAirportID, String codeshare, Integer stops, String equipment, BigDecimal price) {
        this.airline = airline;
        this.airlineID = airlineID;
        this.sourceAirport = sourceAirport;
        this.sourceAirportID = sourceAirportID;
        this.destinationAirport = destinationAirport;
        this.destinationAirportID = destinationAirportID;
        this.codeshare = codeshare;
        this.stops = stops;
        this.equipment = equipment;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public Long getAirlineID() {
        return airlineID;
    }

    public void setAirlineID(Long airlineID) {
        this.airlineID = airlineID;
    }

    public String getSourceAirport() {
        return sourceAirport;
    }

    public void setSourceAirport(String sourceAirport) {
        this.sourceAirport = sourceAirport;
    }

    public Long getSourceAirportID() {
        return sourceAirportID;
    }

    public void setSourceAirportID(Long sourceAirportID) {
        this.sourceAirportID = sourceAirportID;
    }

    public String getDestinationAirport() {
        return destinationAirport;
    }

    public void setDestinationAirport(String destinationAirport) {
        this.destinationAirport = destinationAirport;
    }

    public Long getDestinationAirportID() {
        return destinationAirportID;
    }

    public void setDestinationAirportID(Long destinationAirportID) {
        this.destinationAirportID = destinationAirportID;
    }

    public String getCodeshare() {
        return codeshare;
    }

    public void setCodeshare(String codeshare) {
        this.codeshare = codeshare;
    }

    public Integer getStops() {
        return stops;
    }

    public void setStops(Integer stops) {
        this.stops = stops;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
