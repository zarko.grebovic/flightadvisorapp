package com.org.helper;

import com.opencsv.bean.CsvBindByPosition;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class AirportImportHelper {

        private Long id;

        private String name;

        private String cityName;

        private String countryName;

        private String iata;

        private String icao;

        private Float latitude;

        private Float longitude;

        private Float altitude;

        private String timezone;

        private String dst;

        private String tz;

        private String type;

        private String source;


    public AirportImportHelper(Long id, String name, String cityName, String countryName, String iata, String icao, Float latitude, Float longitude, Float altitude, String timezone, String dst, String tz, String type, String source) {
        this.id = id;
        this.name = name;
        this.cityName = cityName;
        this.countryName = countryName;
        this.iata = iata;
        this.icao = icao;
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
        this.timezone = timezone;
        this.dst = dst;
        this.tz = tz;
        this.type = type;
        this.source = source;

    }

    public String getDst() {
        return dst;
    }

    public void setDst(String dst) {
        this.dst = dst;
    }

    public String getTz() {
        return this.tz;
    }

    public void setTz(String tz) {
        this.tz = tz;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getIcao() {
        return icao;
    }

    public void setIcao(String icao) {
        this.icao = icao;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Float getAltitude() {
        return altitude;
    }

    public void setAltitude(Float altitude) {
        this.altitude = altitude;
    }

    public String getTimezone() {
        return this.timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }





    @Override
    public String toString() {
        return "AirportImportHelper{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cityName='" + cityName + '\'' +
                ", countryName='" + countryName + '\'' +
                ", iata='" + iata + '\'' +
                ", icao='" + icao + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", altitude=" + altitude +
                ", timezone='" + timezone + '\'' +
                ", dst='" + dst + '\'' +
                ", tz='" + tz + '\'' +
                ", type='" + type + '\'' +
                ", source='" + source + '\'' +
                '}';
    }
}
